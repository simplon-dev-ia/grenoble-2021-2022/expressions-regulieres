# Analyse Statistique de Texte

Nous souhaitons analyser le texte de Germinal afin d'obtenir des métriques
statistiques et de pouvoir inférer d'autres analyses plus détaillées.

**Cette activité a une vocation pédagogique : vous ne devez pas avoir recours à des bibliothèques externes calculer les métriques demandées !**

## Analyse Globale

Trouver 2 méthodes pour analyser statistiquement le corpus de texte afin d'obtenir
une "vue" globale du vocabulaire employé dans le texte :

- [ ] Déterminer quelle métrique(s) statistique(s) est(sont) adéquate(s) pour représenter un vocabulaire
- [ ] Trouver une première méthode naive la plus simple possible
- [ ] Trouver une seconde méthode valorisant les termes discriminants

Pour chaque méthode, obtenir un vocabulaire sous la forme d'un DataFrame ainsi que
d'une représentation graphique adaptée.

## Analyse Sémantique

Il serait intéressant de pouvoir naviguer dans le corpus de Germinal plus rapidement,
notamment afin de pouvoir accéder directement aux passages en rapport avec le thème
de _la mine_.

**Votre mission** : trouver un ou plusieurs algorithmes pour localiser avec précision
les passages en rapport avec le thème de _la mine_.

**Conseil** : la plupart des algorithmes d'analyse de texte sont composés de plusieurs
étapes / analyses / algorithmes intermédiaires afin de découper la tâche plus aisément !
Soyez créatifs !
